/*create database TP7

use TP7

create table departamento(
id dec(2) PRIMARY KEY,
nombre_dep varchar(15)
)

create table empleado(
id dec(4) PRIMARY KEY,
apellido varchar(15),
nombre varchar(15),
departamento dec(2) FOREIGN KEY REFERENCES departamento(id),
sueldo decimal(10,2),
jefe varchar(15)
)

INSERT INTO departamento VALUES(1,'Ventas');
INSERT INTO departamento VALUES(2,'Marketing');
INSERT INTO departamento VALUES(3,'Administración');

INSERT INTO empleado VALUES(100,'Borrelli','Facundo', 1, 5800.60,'Pedro');
INSERT INTO empleado VALUES(101,'Perez','Alberto', 3, 18000.78,'Juan');
INSERT INTO empleado VALUES(102,'Nicolás','Martinez', 2, 15060.06,'Pedro');
INSERT INTO empleado VALUES(103,'Matias','Mamanna', 1, 11060.01,'Juan Pablo');
INSERT INTO empleado VALUES(104,'Alejandro','Suarez', 2, 12060.01,'Juan Pablo');
INSERT INTO empleado VALUES(105,'Diego','Lavin', 3, 11060.01,'Pedro');
INSERT INTO empleado VALUES(106,'Leonardo','Pisculichi', 2, 11060.01,'Juan');
INSERT INTO empleado VALUES(107,'Carlos','Barovero', 3, 18090.11,'Juan');*/

--A. Mostrar la cantidad de empleados de cada departamento
	/*select COUNT(*) as 'Dpto ventas' from empleado where departamento in (select id from departamento where nombre_dep='Ventas')
	select COUNT(*) as 'Dpto markt' from empleado where departamento in (select id from departamento where nombre_dep='Marketing')
	select COUNT(*) as 'Dpto Administración' from empleado where departamento in (select id from departamento where nombre_dep='Administración')*/

--B. Calcular el salario total y promedio para cada departamento
	/*select SUM(Sueldo) as 'Salario total Ventas', AVG(Sueldo) as 'Salario Promedio Ventas' from empleado where departamento in (select id from departamento where nombre_dep='Ventas')
	select SUM(Sueldo) as 'Salario total markt', AVG(Sueldo) as 'Salario Promedio markt' from empleado where departamento in (select id from departamento where nombre_dep='Marketing')
	select SUM(Sueldo) as 'Salario total Administración', AVG(Sueldo) as 'Salario Promedio Administración' from empleado where departamento in (select id from departamento where nombre_dep='Administración')*/

--C. Mostrar el departamento con más empleados
	--select TOP 1 count(nombre) as maxi from empleado group by departamento ORDER BY maxi desc
					------------------(corregido)--------------------------

--D. Despliegue los nombres de todos los empleados que ganen menos que el salario promedio de la compañía.
	--select nombre from empleado where sueldo < (select AVG(sueldo) from empleado)

--E. Muestre el resto de los empleados que trabajan en el mismo departamento que los empleados con id 178, 180 o 174 y 
	--son dirigidos por el mismo jefe de los empleados id 178, 180 o 174.
	--select * from empleado where (departamento IN (select departamento from empleado where id=100 or id=101 or id=102) and jefe IN (select jefe from empleado where id=100 or id=101 or id=102))
	-----------------------------------------(corregido)-----------------------------------------------------------------

--F. Mostrar los empleados de los departamentos que tengan más de 3 empleados pordepartamento.
	--select * from empleado where departamento in (select count(*) from empleado group by departamento HAVING COUNT(*) > 3)

--G. Mostrar los datos de un empleado que tengan un sueldo mayor al promedio de los departamento.
	--select * from empleado where sueldo > all(select AVG(sueldo) from empleado group by departamento)
		
--H. Mostrar los empleados que al menos tengan una persona a su cargo.
	--select * from empleado where nombre in (select nombre from empleado where nombre=jefe)

--I. Mostrar todos los departamentos que no tienen ningún empleado.
	--select * from departamento where id not in (select departamento from empleado)
	-----------------------------------------(coregido)-------------------------------------------------------
	