/*create database EJ4

use EJ4
create table libros(
titulo varchar(40),
autor varchar(30),
editorial varchar(15),
precio decimal(5,2),
cantidad dec(3)
)
insert into libros values('El aleph','Borges','Planeta',35,null);
insert into libros values('Martin Fierro','Jose Hernandez','Emece',22.20,200);
insert into libros values('Martin Fierro','Jose Hernandez','Planeta',40,200);
insert into libros values('Antologia poetica','J.L. Borges','Planeta',null,150);
insert into libros values('Aprenda PHP','Mario Molina','Emece',18,null);
insert into libros values('Manual de PHP', 'J.C. Paez', 'Siglo XXI',56,120);
insert into libros values('Cervantes y el quijote','Bioy Casares-J.L.Borges','Paidos',null,100);
insert into libros values('Harry Potter y la piedra filosofal','J.K.Rowling',default,45.00,90);
insert into libros values('Harry Potter y la camara secreta','J.K.Rowling','Emece',null,100);
insert into libros values('Alicia en el pais de las maravillas','Lewis Carroll','Paidos',42,80);
insert into libros values('PHP de la A a la Z',null,null,110,0);
insert into libros values('Uno','Richard Bach','Planeta',25,null);
*/

--A- Queremos saber la cantidad de libros agrupados por editorial:
	--select editorial,SUM(cantidad) from libros group by editorial

--B- Queremos saber la cantidad de libros agrupados por editorial pero considerando s�lo algunos grupos, por ejemplo, los que devuelvan un valor mayor a 2
	--select editorial,SUM(cantidad) from libros group by editorial having sum(cantidad)>2

--C- Queremos el promedio de los precios de los libros agrupados por editorial, pero solamente de aquellos grupos cuyo promedio supere los 25 pesos:
	--select AVG(precio),editorial from libros group by editorial having AVG(precio)>25

--D- Queremos la cantidad de libros, sin considerar los que tienen precio nulo (where), agrupados por editorial (group by), sin considerar la editorial "Planeta" (having):
	--select sum(cantidad) from libros where precio is not null group by editorial having editorial<>'Planeta'

--E- Necesitamos el promedio de los precios agrupados por editorial, de aquellas editoriales que tienen m�s de 2 libros:
	--select AVG(precio) from libros group by editorial having sum(cantidad)>2

--F- Buscamos el mayor valor de los libros agrupados y ordenados por editorial y seleccionamos las filas que tienen un valor menor a 100 y mayor a 30:
	--select editorial,MAX(precio) from libros  group by editorial having max(precio) between 30 and 100 order by editorial