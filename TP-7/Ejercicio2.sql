/*create database inmobiliaria

use inmobiliaria

create table Inquilino(
tipo_doc varchar(5), 
nro_doc dec(10) PRIMARY KEY, 
nombre varchar(15),
apellido varchar(15)
)
create table Due�o(
tipo_doc varchar(5)	,
nro_doc dec(10) PRIMARY KEY, 
nombre varchar(15), 
apellido varchar(15)
)
create table Casa(
id_casa dec(3) PRIMARY KEY, 
due�o dec(10) FOREIGN KEY REFERENCES Due�o(nro_doc),	--dni de due�o 
direccion varchar(20), 
ciudad varchar(20)
)
create table Alquila(
inquilino dec(10) FOREIGN KEY REFERENCES Inquilino(nro_doc),--dni inquilino 
id_casa dec(3) FOREIGN KEY REFERENCES Casa(id_casa),--id_casa de casa 
deuda float check (deuda>=0)
)

INSERT INTO inquilino VALUES ('DNI',12345678,'Alberto','Juarez')
INSERT INTO inquilino VALUES ('DNI',23456789,'Maria','Castro')
INSERT INTO inquilino VALUES ('LU',34567890,'Violeta','Rivas')
INSERT INTO inquilino VALUES ('DNI',45678901,'Candela','Lamela')
INSERT INTO inquilino VALUES ('LU',56789012,'Pamela','Tapia')
INSERT INTO inquilino VALUES ('LU',67890123,'Horacio','Gonett')
INSERT INTO inquilino VALUES ('DNI',78901234,'Monica','Suarez')
INSERT INTO inquilino VALUES ('DU',89012345,'Jose','Ponse')

INSERT INTO due�o VALUES ('DNI',12345679,'Daiana','Juarez')
INSERT INTO due�o VALUES ('DNI',23456749,'Marcelo','Castro')
INSERT INTO due�o VALUES ('LU',34567820,'Viviana','Rivas')
INSERT INTO due�o VALUES ('LU',67890123,'Horacio','Gonett')
INSERT INTO due�o VALUES ('LU',56789012,'Pamela','Tapia')
INSERT INTO due�o VALUES ('DNI',45676901,'Carla','Lamela')
INSERT INTO due�o VALUES ('LU',56784512,'Patricia','Tapia')
INSERT INTO due�o VALUES ('LU',67890523,'Hugo','Gonett')
INSERT INTO due�o VALUES ('DNI',78904234,'Mauro','Suarez')
INSERT INTO due�o VALUES ('DU',89012365,'Juan','Ponse') 

INSERT INTO Casa VALUES (1,23456749,'San Juan 9023','Olavarria')
INSERT INTO Casa VALUES (2,56784512,'Viamonte 9812','Venito Juarez')
INSERT INTO Casa VALUES (3,89012365,'Casanova 892','Olavarria')
INSERT INTO Casa VALUES (4,23456749,'Sarmiento 676','Bahia Blanca')
INSERT INTO Casa VALUES (5,45676901,'San Mart�n 76','Azul')
INSERT INTO Casa VALUES (6,78904234,'San Juan 676','Azul')
INSERT INTO Casa VALUES (7,45676901,'C�rdoba 78','Bahia Blanca')
INSERT INTO Casa VALUES (8,45676901,'San Mart�n 76','Azul')
INSERT INTO Casa VALUES (9,78904234,'Sarmiento 76','Jacinto Arauz')
INSERT INTO Casa VALUES (10,45676901,'San Mart�n 9878','Oriente')

INSERT INTO Alquila VALUES (12345678,1,7800)
INSERT INTO Alquila VALUES (45678901,4,9000)
INSERT INTO Alquila VALUES (23456789,7,8900)
INSERT INTO Alquila VALUES (56789012,3,0)
INSERT INTO Alquila VALUES (78904234,6,0) ---------------------------------(preg)--------------------------------
INSERT INTO Alquila VALUES (78904234,8,3290)---------------------------------(preg)--------------------------------
INSERT INTO Alquila VALUES (23456789,10,5800)
*/

--A. Indique la cantidad de casas por ciudad.
	--select ciudad,COUNT(*) as cantidad from casa group by ciudad 
	
--B. �Cu�nto le deben a Julia Martinez?
	--select SUM(deuda) from alquila where id_casa in (select id_casa from casa where due�o in (select nro_doc from due�o where nombre='Jualia' and apellido='Martinez'))

--C. �Cu�nto debe cada inquilino que su apellido este entre la E y la P?
	--select alquila.deuda from alquila where inquilino in (select nro_doc from inquilino where apellido like '%p%' and apellido like '%e%')
	
--D. �Cu�l es la deuda total para cada due�o?
	--select SUM(alquila.deuda) as 'deuda',Due�o.nombre from ((Alquila inner join Casa on Alquila.id_casa=Casa.id_casa) inner join Due�o on casa.due�o=due�o.nro_doc) group by due�o.nombre 
	----------------------------------(corregido)-----------------------------------------------
--E. Liste todas las personas de la base de datos
	--select nombre,apellido from inquilino union select nombre,apellido from due�o

--F. Indique los due�os que poseen tres o m�s casas.
	--select Nombre,Apellido from due�o where nro_doc in(select due�o from casa group by due�o having COUNT(due�o)>3)
	
--G. Liste los due�os que tengan deudores en todas sus casas.
	--select * from due�o where nro_doc in(select due�o from casa where id_casa in(select id_casa from alquila where deuda<>0))

--H. Mostrar los 5 inquilinos que tienen m�s deudas.
	--select top 5 Inquilino.nombre, alquila.deuda from Inquilino inner join Alquila on Alquila.inquilino=inquilino.nro_doc order by Alquila.deuda desc
	----------------------------------(corregido)---------------------------------
--I. Mostrar todas las personas que sean due�os e inquilinos.
	--select * from due�o,inquilino where inquilino.nro_doc=due�o.nro_doc

--J. Mostrar todas las personas que sean due�os y no inquilinos.
	--select Due�o.nombre from ((Alquila inner join Casa on casa.id_casa=Alquila.id_casa) inner join Due�o on casa.due�o=due�o.nro_doc) where alquila.inquilino<>casa.due�o
	----------------------------------(corregido)------------------------------------