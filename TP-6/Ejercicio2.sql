
--Crear una vista en la que obtenga los campos de titulo, isbn, editorial, paginas y a�o de
--edici�n, que correspondan a una editorial en especifico y la guarda con el nombre libroseditorial
	create view libroseditorial as 
	select titulo,isbn,editorial,paginas,a�oedici�n from libros
	where editorial='enespecifico'
	
--Crear una vista llamada Oficinas_Este donde se muestran las oficinas que est�n en la regi�n Este
	create view Oficinas_Este as
	select * from oficinas
	where region='Este'

--Eliminar la Vista Oficinas_Este
	drop view Oficinas_Este