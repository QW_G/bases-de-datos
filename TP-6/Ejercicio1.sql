/*create database TP6

use TP6

create table secciones(
codigo dec(2) not null PRIMARY KEY,
nombre varchar(20),
sueldo dec(5,2) check (sueldo>=0)
);

create table empleados(
legajo dec(5) PRIMARY KEY,
documento varchar(8),
sexo varchar(1),
apellido varchar(20),
nombre varchar(20),
domicilio varchar(30),
seccion dec(2) not null,
cantidadhijos dec(2),
estadocivil varchar(10),
fechaingreso date,
FOREIGN KEY (seccion) references secciones(codigo)
);

insert into secciones values(1,'Administracion',300);
insert into secciones values(2,'Contadur�a',400);
insert into secciones values(3,'Sistemas',500);

insert into empleados values(100,'22222222','f','Lopez','Ana','Colon 123',1,2,'casado','10/10/1990');
insert into empleados values(102,'23333333','m','Lopez','Luis','Sucre 235',1,0,'soltero','02/10/1990');
insert into empleados values(103,'24444444','m','Garcia','Marcos','Sarmiento 1234',2,3,'divorciado','12/07/1998');
insert into empleados values(104,'25555555','m','Gomez','Pablo','Bulnes 321',3,2,'casado','10/09/1998');
insert into empleados values(105,'26666666','f','Perez','Laura','Peru 1254',3,3,'casado','05/09/2000');*/

--1- Crear la vista "vista_empleados", que es resultado de una combinaci�n en la cual semuestran 3 campos: Apellido, Legajo, Domicilio
	--create view vista_empleados as select apellido,legajo,domicilio from empleados
--2- Realizamos una consulta a la vista como si se tratara de una tabla
	--select * from vista_empleados
--3- Realizar una nueva vista V_Emp donde muestre los nombre y legajos de los empleados Masculinos
	--create view V_Emp as select nombre,legajo,fechaingreso from empleados where sexo='m'
--4- Eliminamos la vista "vista_empleados"
	--drop view vista_empleados
--5- Modificamos una fecha en la tabla "empleados" y luego consultamos la vista para verificar que est� actualizada
	--Update empleados set fechaingreso='10/05/19' where legajo=102
	--select * from V_Emp