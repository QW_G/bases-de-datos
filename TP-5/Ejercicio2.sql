/*create database Escuela

CREATE TABLE Alumnos(
id int not null,
nombre varchar(10) NOT NULL,
apellido varchar(10) NOT NULL,
telefono varchar(30),
PRIMARY KEY (id)
)
CREATE TABLE Notas(
Id_Nota int not null,
Id_Alu int not null,
materia varchar(10) not null,
nota int not null check (Nota>= 1 AND Nota<=10),
PRIMARY KEY(Id_Nota),
FOREIGN KEY (Id_Alu) REFERENCES Alumnos(id) 
);

INSERT INTO Alumnos VALUES(1,'Juan','Garcia',949494949);
INSERT INTO Alumnos VALUES(2,'Maria','Alvarez',null);
INSERT INTO Alumnos VALUES(3,'Carlos','Perez',6458544);
INSERT INTO Alumnos VALUES(4,'Alberto','Jimenez',null);
INSERT INTO Alumnos VALUES(6,'Valentino','Alvarez',155283478);
INSERT INTO Alumnos VALUES(5,'Vanesa','Galera',912552522);
INSERT INTO Alumnos VALUES(8,'Sergio','Molina',5464465656);
INSERT INTO Alumnos VALUES(7,'Felipe','Garcia',155283498);-------------LE CAMBIE PRIMARY KEY

insert into notas values(1, 2, 'Matematica', 8)
insert into notas values(2, 2, 'Lengua', 9)
insert into notas values(3, 5, 'Sociales', 7)
insert into notas values(4, 3, 'Ingles', 6)
insert into notas values(5, 3, 'Estadis', 2)
insert into notas values(6, 1, 'Sociales', 3)
insert into notas values(7, 4, 'Matematica', 1)
insert into notas values(8, 7, 'Lengua', 5)
insert into notas values(9, 9, 'Sociales', 10)
insert into notas values(10, 1, 'Matematica', 8)
*/

--1- Mostrar todos los alumnos
--	select * from alumnos

--2- Mostrar los alumnos que tienen materias aprobadas (Es decir mayor e igual a 7)
--	select * from alumnos inner join notas on id = Id_Alu where nota > 6

--3- Mostrar los alumnos donde el nombre contenga una �O�
--	select * from alumnos where nombre like '%o%'

--4- Calcular el Precio promedio de las notas
--	select AVG(nota) as 'pomedio notas' from notas

--5- Cu�l es la Nota M�xima y cual la M�nima que se sac� el alumno �Galera�--
--	select max(nota) as 'Nota Maxima', min(nota) as 'Nota Minima' from notas inner join alumnos on id = id_alu where apellido = 'Galera' ---------------------(preg)---
	
--6- Mostrar los alumnos que tengan alguna de estas notas 8,4,6,9
--	select * from alumnos inner join notas on  id_alu = id where nota=8 or nota=4 or nota=6 or nota=9

--7- Mostrar cada uno de los alumnos con sus respectivas notas y el nombre de la materia
--	select nombre,materia,nota from alumnos inner join notas on id_alu=id																----------------------(preg)---

--8- Mostrar los primeros 10 alumnos que tengan un 8 como nota
--	select TOP 10 * from alumnos inner join notas on id_alu=id where nota=8																----------------------(preg)---

--9- Mostrar los nombres y notas de alumnos desaprobados ordenados por la mayor nota desaprobada
--	select nombre,nota from alumnos inner join notas on id_alu=id where nota<7 order by nota desc

--10- Actualizar los datos de Sergio Molina y ponerle nota 9
--	update notas set nota=9 where id_alu=(select id from alumnos where nombre='Sergio' and apellido='Molina')

--11- Mostrar los alumnos que no tengan apellidos repetidos
--	select distinct apellido from alumnos

--12- Eliminar a los alumnos cuyo apellido sea Garcia											------------------------------------(no.pude)-------------(preg)------
--	alter table notas drop constraint id_alu
--	alter table notas add constraint id_alu foreign key (id_alu) references alumnos(id) on delete cascade;
--	delete * from alumnos where apellido = 'Garcia'

