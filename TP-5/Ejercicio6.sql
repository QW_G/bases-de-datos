/*
Proveedor (nroProv, nomProv, categor�a, ciudProv)
Art�culo (nroArt, descripci�n, ciudArt, precio)
Cliente (nroCli, nomCli, ciudCli)
Pedido (pedNnro, nroProv, NroArt, nroCli, cantidad, precioTotal)
*/

--1. Hallar el c�digo (nroProv) de los proveedores que proveen el art�culo al46.
	select nroProv from Proveedor where nroProv in (
		select nroProv from Pedido where nroArt='a146')

--2. Hallar los clientes(nomProv) que solicitan art�culos provistos por p015.
	select * from Cliente where nroCli in (
		select nroCli from Pedido where nroProv='p015')

--3. Hallar los clientes que solicitan alg�n �tem provisto por proveedores con categor�a mayor que 4.
	select * from CLiente where nroCli in (
		select nroCli from Pedido where nroProv in (
		select nroProv from Proveedor where categoria>4))

--4. Hallar los �tem pedidos por clientes de Rosario.
	select * from Articulo where nroArt in (
		select nroArt from Pedido where nroCli in (
		select nroCli from Cliente ciudCli='Rosario'))

--5. Hallar los pedidos en los que un cliente de Rosario solicita art�culos producidos en la ciudad de Mendoza.
	select * from Pedido where nroCli in (
		select nroCli from Cliente where ciudCli='Rosario')
		and nroArt in (
		select nroArt from Articulo where ciudArt='Mendoza')
		
--6. Hallar los pedidos en los que el cliente c23 solicita art�culos no solicitados por el cliente c30.
	select * from Pedido where nroCli=
		EXISTS(select nroCli from Cliente where nroCli='c23')
		AND NOT EXISTS(select nroCli from Cliente where nroCli='c30')
			
--7. Hallar los nombres de los proveedores cuya categor�a sea mayor que la de todos los proveedores que proveen el art�culo "cuaderno".
	select nomProv from Proveedor where categoria> ALL (
		select categoria from Preveedor where nroProv in (
		select nroProv from Pedido where nroArt in (
		select nroArt from Articulo where descripcion='cuaderno')))

--8. Hallar los proveedores que proveen el art�culo m�s caro comprado alguna vez por un cliente de la ciudad de Mendoza.
	select * from Proveedor where nroProv IN (
		select nroProv from Pedido where codArt IN (
		select codArt,MAX(Precio) from Articulo)
		and nroCli IN (
		select nroCli from Cliente where CuidCli='Mendoza'))
		
--9. Hallar los clientes que han pedido dos o m�s art�culos distintos.
	select * from Cliente where nroCli IN (
		select nroCli from Pedido where DISTINCT(pedNro and nroArt)>3)

--10. Hallar los proveedores que no tienen ning�n pedido en los que el cliente es de la ciudad de Mendoza y el art�culo es producido en San Juan.
	select * from Proveedor where nroProv = NOT EXISTS(
		select nroProv from Pedido where nroCli IN (
			select nroCli from Cliente where cuidCli='Mendoza'))
		and nroProv IN (
		select nroProv from Pedido where nroArt=(
			select norArt from Articulo where cuidArt='San Juan'))		

--11. Hallar los clientes que compran art�culos cuyo precio promedio es superior a $ 100.
	select * from Cliente where nroCli IN (
		select nroCli from Pedido where nroArt IN (
		select nroArt from Articulo where precio > ALL(
		select avg(precio) from Articulo)))

--12. Hallar los proveedores que venden todos los art�culos cuyo precio es superior al precio promedio de los art�culos que se producen en La Plata.
	select * from Proveedor where nroProv IN (
		select nroProv from Pedido where nroArt IN ( 
		select nroArt from Articulo where precio > ALL(
		select avg(precio) from Articulo where cuidArt='La Plata' )))

--13. Hallar la cantidad de art�culos diferentes provistos por cada proveedor que provee a todos los clientes de Jun�n.
	select count(nroArt),count(nroProv) from Pedido where nroCli IN (
		select nroCli from Cliente where cuidCli='Junin')

--14. Hallar los proveedores que han provisto m�s de 1000 unidades entre los art�culos AOO1 y Al00.
	select * from Proveedor where norProv IN (
		select nroProv from Pedido where 1000 < ANY(
		select sum(cantidad) from pedido where nroArt between A001 and A100))