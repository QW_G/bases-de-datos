/*create database ejercicio10

use ejercicio10

create table Empleado(
codigo int,				
nombre varchar(10),
apellido varchar(15),
cargo varchar(15),
fecha_nac date,
salario float,
comision float,
PRIMARY KEY (codigo)
)

insert into Empleado values(2030, 'Adrian', 'Montero', 'Vendedor', '10-02-1975', 12500, 20)
insert into Empleado values(2120, 'H�ctor', 'Rocha', 'Analista', '12-05-1983' ,16800, 0)
insert into Empleado values(2530, 'Jos�', 'P�rez', 'Director', '14-07-1965', 19000, 30)
insert into Empleado values(3526, 'Carmen', 'Castillo', 'Vendedor', '12-09-1980' ,16000, 50)
insert into Empleado values(3652, 'Alfonso', 'P�rez', 'Analista', '03-02-1980', 16200, 80)
insert into Empleado values(5214, 'Luis', 'Montero', 'Contador', '06-11-1975', 17000, 2)
insert into Empleado values(8554, 'Manuel', 'Mart�nez', 'Vendedor', '05-10-1983', 16200, 25)
insert into Empleado values(2640, 'Mart�n', 'Montero', 'Analista', '12-06-1988', 14200, 78)	*/

--a) Mostrar los nombres completos de los empleados ordenados alfab�ticamente desde la Z hasta la A.
	--select  apellido,nombre from Empleado order by apellido desc
--b) Mostrar los sueldos con su comisi�n correspondiente, legajo y nombre completos de los empleados que son �Vendedores� ordenados por sueldos (de mayor a menor) y despu�s pornombre completo (de A� Z).
	--select comision,salario,codigo,nombre from Empleado where cargo='Vendedor' order by salario desc ,nombre asc
--c) Mostrar datos principales de los empleados cuyos nombres terminen con la letra �n� y tengan apellido terminado en �o�.
	--select * from Empleado where nombre like '%n' and apellido like '%o'
--d) Mostrar nombre completo, c�digo y oficio de empleados que tengan un salario entre 15000 y 17000 pesos.
	--select nombre,codigo,cargo from Empleado where salario between 15000 and 17000
--e) Mostrar los datos de los empleados que sean Analista y a su vez tengan c�digo de mayor a 3000.
	--select * from Empleado where cargo='Analista' and codigo>3000
--f) Calcular el salario m�ximo, m�nimo y promedio de los empleados.
	--select MAX(salario) as 'maximo', MIN(salario) as 'minimo', AVG(salario) as 'promedio' from Empleado
--g) Contar la cantidad de empleados que trabajan tienen comisi�n.
	--select COUNT(nombre) from Empleado where comision<>0
--h) Calcular el n�mero de empleados que no tienen comisi�n.
	--select COUNT(*) from Empleado where comision = 0
--i) Mostrar c�digo, nombre completo y fecha de nacimiento de los empleados ordenados por fecha de nacimiento.
	--select codigo,nombre,fecha_nac from empleado order by fecha_nac
--j) Mostrar los datos de los empleados con el a�o en que nacieron que est�n en los cargo Director o Contador.
	--select * from empleado where cargo = 'Director' or cargo = 'Contador