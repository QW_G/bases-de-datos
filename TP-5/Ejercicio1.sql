/*create database ProductosBD

CREATE TABLE productos (
Codigo varchar(3),
nombre varchar(30),
precio decimal(6,2),
fechaalta date,
PRIMARY KEY (codigo)
);

INSERT INTO productos VALUES ('a01','Afilador', 2.50, '2007-11-02');
INSERT INTO productos VALUES ('s01','Silla mod. ZAZ', 20, '2007-11-03');
INSERT INTO productos VALUES ('s02','Silla mod. XAX', 25, '2007-11-03');*/

--1.Seleccionar todos los productos
--	select * from productos

--2.Seleccionar los productos cuyo nombre sea Afilador	
--	select * from productos where nombre='Afilador'

--3.Seleccionar los productos donde el nombre contenga una �S�
--	select * from productos where nombre like '%s%'

--4.Calcular el Precio promedio de los productos
--	select AVG(precio) as 'Precio Promedio' from productos

--5.Actualizar el dato del Afilador y ponerle 3.50
--	update productos set precio = 3.5 where nombre = 'Afilador'

--6.Eliminar el producto con codigo s02
--	delete from productos where codigo = 's02'

--7.Mostrar los productos con un incremento de IVA del 21% en el precio
--	select nombre,(precio*1.2) as 'incremento IVA' from productos	------------------------------------------------------(preg)-------------------
	
--8.Mostrar los productos donde el nombre tenga solo 3 caracteres (SUBSTRING)
--	select * from productos where nombre = SUBSTRING(nombre,1,2)	------------------------------------------------------(preg)-------------------

--9.Contar cuantos productos tengo
--	select COUNT(*) as 'cantidad' from productos 

--10.Mostrar el a�o correspondiente a cada producto (YEAR)
--	select nombre,YEAR(fechaalta) as 'a�o entrada' from productos