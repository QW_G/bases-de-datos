/*create database prueba2

create table vtaxciudad( 
nom_ciudad varchar(25), 
tot_ventas smallmoney )

create table vtaxvendedor( 
vendedor varchar(25), 
tot_ventas smallmoney, 
sucursal int )

INSERT INTO vtaxciudad VALUES ('Soyapango',3456.99)
INSERT INTO vtaxciudad VALUES ('Santa Ana',12456.23)
INSERT INTO vtaxciudad VALUES ('Ciudad Barrios',6356.99)
INSERT INTO vtaxciudad VALUES ('Apopa',956.00)
INSERT INTO vtaxciudad VALUES ('San Bartolo',13756.45)

INSERT INTO vtaxvendedor VALUES ('Efrain Burgos',345.56,1)
INSERT INTO vtaxvendedor VALUES ('Carlos Olmos',2345.00,1)
INSERT INTO vtaxvendedor VALUES ('Efrain Burgos',509.87,2)
INSERT INTO vtaxvendedor VALUES ('Karla Alas',3123.05,3)
INSERT INTO vtaxvendedor VALUES ('Carlos Olmos',323.76,3)
INSERT INTO vtaxvendedor VALUES ('Efrain Burgos',800.00,2)
INSERT INTO vtaxvendedor VALUES ('Carlos Olmos',654.43,2)
INSERT INTO vtaxvendedor VALUES ('Karla Alas',123.34,4)
INSERT INTO vtaxvendedor VALUES ('Carlos Olmos',450.34,5)
*/
															--------------------------------------------(preg todo, si las consignas est�n correctas)-----------
--1- Calcular el Total M�ximo de vtaxciudad
--	select MAX(tot_ventas) from vtaxciudad							

--2- Calcular el Total M�nimo de vtaxvendedor
--	select MIN(tot_ventas) from vtaxvendedor

--3- Calcular el Total Promedio vtaxciudad
--	select AVG(tot_ventas) as 'prom ventas' from vtaxciudad

--4- Calcular la Suma Total de todas las vtasxvendedor
--	select SUM(tot_ventas) as 'total' from vtaxvendedor	

--5- Calcular los Totales vtaxciudad que est�n entre $500 y $1500
--	select SUM(tot_ventas) as 'tot' from vtaxciudad where tot_ventas between 500 and 1500

--6- Contar cuantos vtaxciudad tengo con valor mayor a 4500
--	select COUNT(tot_ventas) as 'cuenta' from vtaxciudad where tot_ventas > 4500