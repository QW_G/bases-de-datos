/*create database DatosEmpresa

create table empleado(
empleado_id int identity(1,1), 
nombre varchar(20),
apellido varchar(20),		---le agregue 20 por error de sq (los datos se truncan) (preg)--------
sueldo decimal,
legajo int,
sector varchar(30),
PRIMARY KEY (empleado_id)
)

insert into empleado values('Facundo','Borrelli',2300,56775,'') ---------le agregue las comillas al final
insert into empleado values('Carlos','Perez',2454.34,113302,'')
insert into empleado values('Alberto','Sanchez',null,113302,'')
insert into empleado values('Alberto','Sanchez',2300,113303,'')
*/


--1- Calcular Sueldo M�ximo
--	select MAX(sueldo) from empleado

--2- Calcular Sueldo M�nimo
--	select MIN(sueldo) from empleado

--3- Calcular Sueldo Promedio
--	select AVG(sueldo) from empleado

--4- Calcular la Suma Total de todos los Sueldos
--	select SUM(sueldo) from empleado

--5- Calcular los Sueldos que est�n entre $2300 y $2700
--	select nombre,sueldo from empleado where sueldo between 2300 and 2700

--6- Agregar Sector a todos los Empleados
--	update empleado set sector = 'Ventas' where empleado_id<3
--	update empleado set sector = 'RRHH' where empleado_id>2

--7- Mostrar los Empleados que corresponden al Sector Ventas
--	select * from empleado where sector = 'Ventas'

--8- Mostrar los Sueldos que son Nulos
--	select * from empleado where sueldo is null

--9- Mostrar la cantidad de caracteres de cada apellido
--	select apellido, LEN(apellido) as 'carcater' from empleado ------------------------------------(preg)-----------------------------------------