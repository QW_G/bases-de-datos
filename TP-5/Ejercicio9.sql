/*create database Concesionario

create table Clientes(
CodCli int PRIMARY KEY, 
NombreCli varchar(20), 
apeCli varchar(20), 
dirCli varchar(20), 
poblacion varchar(10), 
codigoPostal int, 
provincia varchar(30), 
telefo0 varchar(10), 
fechaNac date
)

create table CochesVendidos(
Matricula varchar(10)PRIMARY KEY, 
marca varchar(15),
modelo varchar(20), -------------------------les puse de 20 por error 
color varchar(10), 
precio decimal(19,2), 
extrasInstalados TEXT, --------------preg
CodCli int
--FOREIGN KEY (CodCli) REFERENCES Clientes(CodCli) 0 lo ejecuto por posible error preguntar, (primero hay que complear la original y despues las otras)
)

create table Revision(
NroRevision int IDENTITY(1,1) PRIMARY KEY, --cambie autonumerico con identity 
cambioAceite bit, 
cambioFiltro bit,
revisionFrenos bit, 
otros text,				--cambie memo por text
Matricula varchar(10),
FOREIGN KEY (Matricula) REFERENCES CochesVendidos(Matricula)-- mismo error
)
*//*
ALTER TABLE	CochesVendidos ALTER COLUMN extrasInstalados text
ALTER TABLE	CochesVendidos ALTER COLUMN modelo varchar(20)
ALTER TABLE CochesVendidos DROP CONSTRAINT CodCli

insert into CochesVendidos values('V2360OX','Opel','Corsa 1.2 Sport','Azul',21000,'Antena el�ctrica',100)
insert into CochesVendidos values('V1010PB', 'Ford', 'Probe 2.0 16V', 'Blanco', 28600,'', 101)
insert into CochesVendidos values('V4578OB', 'Ford', 'Orion 1.8 Ghia', 'Negro' ,26000, 'Aire Acondicionado', 105)
insert into CochesVendidos values('V7648OU', 'Citroen', 'Xantia 16V', 'Negro', 24800,' Airbag', 225)
insert into CochesVendidos values('V3543NC',' Ford', 'Escort 1.6 Ghia',' Rojo', 25000,'', 260)
insert into CochesVendidos values('V7632NX','Citroen', 'Zx Turbo-D', 'Rojo', 28000,' Aire Acondicionado, Airbag', 289)
insert into CochesVendidos values('V8018LJ','Ford', 'Fiesta 1.4 CLX', 'Azul', 19500, 'Elevalunas el�ctricos', 352)
insert into CochesVendidos values('V2565NB','Renault', 'Clio 1.7 S', 'Blanco', 21000,'', 390)
insert into CochesVendidos values('V7642OU','Ford', 'Mondeo 1.8 GLX', 'Blanco', 31000,'', 810)
insert into CochesVendidos values('V1234LC','Audi', '100 2.3', 'Verde', 35100, 'Climatizador', 822)
insert into CochesVendidos values('V9834LH','Peugeot', '205 GTI', 'Rojo', 24500,'', 860)
*/
--select * from Revision
/*
Insert into Revision(cambioAceite,cambioFiltro,revisionFrenos,otros,Matricula) values (1,0,0,'Revisar luces','V7632NX')
Insert into Revision(cambioAceite,cambioFiltro,revisionFrenos,otros,Matricula) values (1, 1, 0, 'Cambiar limpias', 'V7632NX')
Insert into Revision(cambioAceite,cambioFiltro,revisionFrenos,otros,Matricula) values (0, 1, 1, 'Arreglar alarma', 'V4578OB')
Insert into Revision(cambioAceite,cambioFiltro,revisionFrenos,otros,Matricula) values (0, 1, 1, 'Ajustar tablero', 'V2360OX')
Insert into Revision(cambioAceite,cambioFiltro,revisionFrenos,otros,Matricula) values (1, 1, 1, 'Cambiar limpias, revisar luces', 'V2565NB')
Insert into Revision(cambioAceite,cambioFiltro,revisionFrenos,otros,Matricula) values (0, 0, 1, 'Cambiar luz interior', 'V7648OU')
Insert into Revision(cambioAceite,cambioFiltro,revisionFrenos,otros,Matricula) values (1, 1, 0,'', 'V2565NB')
Insert into Revision(cambioAceite,cambioFiltro,revisionFrenos,otros,Matricula) values (0, 0, 0,'', 'V8018LJ')
Insert into Revision(cambioAceite,cambioFiltro,revisionFrenos,otros,Matricula) values (1, 0, 1, 'Regular encendido', 'V3543NC')
Insert into Revision(cambioAceite,cambioFiltro,revisionFrenos,otros,Matricula) values (0, 1, 0, 'Reparar puerta delantera', 'V8018LJ')
Insert into Revision(cambioAceite,cambioFiltro,revisionFrenos,otros,Matricula) values (0, 0, 0,'', 'V3543NC')
Insert into Revision(cambioAceite,cambioFiltro,revisionFrenos,otros,Matricula) values (1, 1, 1,'', 'V1234LC')
Insert into Revision(cambioAceite,cambioFiltro,revisionFrenos,otros,Matricula) values (0, 1, 0, 'Cambiar limpias', 'V9834LH')
Insert into Revision(cambioAceite,cambioFiltro,revisionFrenos,otros,Matricula) values (0, 1, 0,'', 'V1010PB')
*/

--1-Mostrar �nicamente los campos Matricula, Marca y Modelo de los Coches vendidos
	--select Matricula,Marca,Modelo from cochesvendidos

--2-Agregar precio a la consulta anterior y mostrar solo quellos Coches que sean Ford
	--select Matricula,Marca,Modelo,precio from cochesvendidos where marca='Ford'
	

--3-Mostrar los Coches cuyo precio sea superior a 2600000
	--select * from cochesvendidos where precio>2600000

--4-Mostrar Apellidos y Poblacion de los Clientes que hayan comprado Ford o Citroen, deben aparecer por orden alfabetico dentro de cada poblaci�n
	select apeCli, poblacion from clientes inner join CochesVendidos on clientes.CodCli=cochesvendidos.CodCli where marca='Ford' or marca='Citroen' order by poblacion
	
--5-Mostrar cuantos coches se vendieron, sin contar los que son marca Citroen
	--select * from CochesVendidos where Marca<>'Citroen'

--6-Mostrar cuantas revisiones con cambio de aceite, cuantas con cambio de filtros
	--select * from revision where cambioAceite=1
	--select * from revision where cambioFiltro=1