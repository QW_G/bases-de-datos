/*
Almac�n (Nro, Responsable)
Art�culo (C�d-art, Descripci�n, Precio)
Material (C�d-mat, Descripci�n)
Proveedor (C�d_prov, Nombre, Domicilio, Ciudad)
Tiene (Nro, C�d-art, Cantidad)
Compuesto-por (C�d-art, C�d_mat)
Provisto_por (C�d-Mat, C�d_prov)
*/

--1. Hallar los nombres de los proveedores de la ciudad de La Plata.
	select nombre from Proveedor where Ciudad = 'La Plata'

--2. Hallar los materiales, c�digo y descripci�n, provistos por proveedores de la ciudad de Rosario.
	select * from Material where C�d-mat IN (select C�d-mat from Pervisto_por where C�d_prov IN (select C�d_prov from Proveedor where Ciudad='Rosario'))

--3. Hallar c�digos y descripciones de los art�culos compuestos por al menos un material provisto por el proveedor L�pez.
	select C�d-art,Descripci�n from Art�culo where C�d-art IN (
		select C�d-art from Compuesto-por where C�d_mat IN (			------(preg)-----------
		select C�d_mat from Provisto_por where C�d_prov = ANY (
		select C�d_prov from Proveedor where Nombre='Lopez'))) 
		
--4. Hallar los c�digos y nombres de proveedores que proveen al menos un material que se usa en alg�n art�culo cuyo precio es mayor que $100.
	select C�d_prov,Nombre from Proveedores where C�d_prov IN (
		select C�d_prov from Provisto_por where C�d_mat IN (	
		select C�d_mat from Compuesto-por where C�d-art= ANY(
		select C�d-art from Art�culo where Precio>100))) 
	
--5. Hallar el o los c�digos de los art�culos de mayor precio.
	select MAX(precio),C�d-art from Art�culo

--6. �dem 5 de menor precio.
	select MIN(precio),C�d-art from Art�culo

--7. Hallar los n�meros de almacenes que tienen todos los art�culos que incluyen el material con c�digo 123.
	select Nro from Tiene where C�d-art = ALL(
		select C�d-art from Compuesto-por where C�d-mat=123)
		
--8. Hallar los nombres de los proveedores de todos los materiales que componen el art�culo A303.
	select Nombre from Proveedor where C�d_Prov IN (
		select Cod_Prov from Provisto_por where C�d-Mat = ALL(
		select C�d-Mat from Compuesto-por where C�d-art=A303))

--9. Hallar los proveedores (c�digo) que proveen al menos un material para todo art�culo.
	select C�d_Prov from Provisto_por where C�d-Mat = ANY(
		select C�d-Mat from Compuesto-por where ALL C�d-art)

--10. Hallar los c�digos y descripciones de los art�culos que tienen al menos un material que no provee ning�n proveedor.
	select C�d-art,Descripci�n from Art�culo where C�d-art IN (
		select C�d-art from Compuesto-por where EXISTS(        -----------------(PREG)-----------------------
		select C�d-Mat from Provisto_por where C�d_prov is NULL))
		
Almac�n (Nro, Responsable)
Art�culo (C�d-art, Descripci�n, Precio)
Material (C�d-mat, Descripci�n)
Proveedor (C�d_prov, Nombre, Domicilio, Ciudad)
Tiene (Nro, C�d-art, Cantidad)
Compuesto-por (C�d-art, C�d_mat)
Provisto_por (C�d-Mat, C�d_prov)

--11. Hallar para cada almac�n el precio m�nimo, m�ximo y promedio de los art�culos que tiene.
	select MIN(precio),MAX(precio),AVG(precio),Responsable from Art�culo where C�d.art IN (
		select C�d-art from Tiene where Nro = ALL(	
		select Nro, Responsable from Almac�n)
	
--12. Listar para cada almac�n el stock valorizado (c�digo y descripci�n de cada art�culo,cantidad, precio unitario, valor total del art�culo).
	select C�d-art, Descripci�n, Precio, Responsable  from Art�culo where C�d.art IN (
		select C�d-art from Tiene where Nro = ALL(	
		select Nro, Responsable from Almac�n)) 
		
--13. Listar el stock valorizado agregado (independiente del almac�n) para todos los art�culos cuya existencia supera 100 unidades.
	select * from Articulos where C�d-art IN (
		select C�d-art from Tiene where Cantidad>100)

--14. Hallar los art�culos cuyo precio es superior a $50 y que est�n compuestos por m�s de tres materiales.
	select * from Ariculos where Precio>50 and EXISTS(select C�d-art from Compuesto-por where count(Cod-art)>3)
	
--15. Listar los materiales que componen los art�culos cuyo precio es superior al precio promedio de los art�culos del almac�n nro. 2.
	select * from Material where C�d-mar in (
		select Cod-mat from Compuesto-por where Cod-art in (
		select Cod-art from Articulo where Precio>(
		select AVG(Precio) from Articulo where Cod-art in (
		select Cod-art from Tiene where Nro=2))))