/*create database E11

use E11

Create table profesores(
documento varchar(8) not null,
nombre varchar (30),
domicilio varchar(30),
primary key(documento)
);
Create table alumnos(
documento varchar(8) not null,
nombre varchar (30),
domicilio varchar(30),
primary key(documento)
);

Insert into alumnos values('30000000','Juan Perez','Colon 123');
Insert into alumnos values('30111111','Marta Morales','Caseros 222');
Insert into alumnos values('30222222','Laura Torres','San Martin 987');
Insert into alumnos values('30333333','Mariano Juarez','Avellaneda 34');
Insert into alumnos values('23333333','Federico Lopez','Colon 987');
Insert into profesores values('22222222','Susana Molina','Sucre 345');
Insert into profesores values('23333333','Federico Lopez','Colon 987');
*/

--el comando union muestra todo en la misma tabla, agregando 'union all' se muestran registros duplicados
select nombre, domicilio from alumnos
union
select nombre, domicilio from profesores;

--ordenados por domicilio
select nombre, domicilio from alumnos
union
select nombre, domicilio from profesores
order by domicilio;

--Podemos agregar una columna extra a la consulta con el encabezado "condicion" en la que aparezca el literal "profesor" o "alumno" seg�n si la persona es uno u otro:
select nombre, domicilio, 'alumno' as condicion from alumnos
union
select nombre, domicilio,'profesor' from profesores
order by condicion;